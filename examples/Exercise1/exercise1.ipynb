{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Anisotropic constant pressure MD¶\n",
    "\n",
    "## Summary\n",
    "\n",
    "This exercise studies a well-known phase transition in potassium chloride, see ref. (Parrinello and A. Rahman, Polymorphic transitions in alkali halides. A molecular dynamics study, Journal de Physique Colloques, 42 C6, p. C6, 1981, doi: 10.1051/jphyscol:19816149, URL https://hal.archives-ouvertes.fr/jpa-00221214.), using constant pressure molecular dynamics. The objective is to develop the best practice in using such algorithms and to learn how phase transitions can be induced, detected and monitored in a simulation.\n",
    "\n",
    "## Background\n",
    "\n",
    "Potassium chloride at ambient temperature and pressure adopts the cubic rocksalt structure, in which each ion is surrounded by six ions of opposite charge in an octahedral arrangement. Under high pressure this structure transforms to something more close packed - the so-called caesium chloride structure, where the nearest neighbour coordination rises to eight ions. (Using the model potential adopted here, this occurs at about 1.4 GPa.) In this exercise the student will have the opportunity to see this phase transition using the method of anisotropic constant pressure molecular dynamics. Commencing with the rocksalt crystal structure and applying a fixed external pressure it is possible to induce the phase transition in a simulation. Similarly it is possible to see the reverse transition back to rocksalt. However it is not necessarily trivial to make these transitions happen in any given simulation (though you may be lucky the first time!) Your task will be to find the conditions under which the phase transition occurs. This will not be entirely a matter of finding the right conditions of temperature and pressure, but will also involve setting up the control parameters for the simulation so as to encourage the phase transition to occur. (Even if the transformation is thermodynamically permitted, it does not follow that it will happen in the lifetime of a simulation.)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setup\n",
    "\n",
    "let us start by loading needed modules to run dlpoly and load prepared inputs. We assume you followed previous instructions to install dlpoly and install jupyter kernel.\n",
    "\n",
    "list the input files\n",
    "\n",
    "You will see the files CONTROL, FIELD and CONFIG. The last of these is a crystal of potassium chloride at ambient temperature and pressure (i.e. in the rocksalt structure). You should proceed as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "load needed python modules. Before loading select from Kernel-> Change Kernel -> dlpoly\n",
    "\n",
    "also we will add soem helper function showrdf to allow us to visualise the rdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dlpoly import DLPoly\n",
    "from dlpoly.rdf import rdf\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def showrdf(loc):\n",
    "    m = rdf(loc)\n",
    "    for i in range(len(m.labels)):\n",
    "        plt.plot(m.x, m.data[i,:,0],label = \"-\".join(m.labels[i]))\n",
    "    plt.xlabel(\"r [Å])\")\n",
    "    plt.ylabel(\"gofr [a.u.])\")\n",
    "    plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "load the control, field and config and setup a working directory called w40 in this case.\n",
    "second line is running dlpoly for you and the third line will show your the rdf.\n",
    "Last three lines indicate at what pressure and temperature the simulation is run (check the manual for the units) and the statistical ensemble we use to sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dlPoly = DLPoly(control=\"CONTROL\", config=\"CONFIG\",\n",
    "                field=\"FIELD\", workdir=\"w40\")\n",
    "dlPoly.run(numProcs = 1)\n",
    "showrdf(\"w40/RDFDAT\")\n",
    "print(\"Pressure: {} unit\".format(*dlPoly.control.pres))\n",
    "print(\"Temperature: {} unit\".format(dlPoly.control.temp))\n",
    "print(dlPoly.control.ensemble)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "you can now open REVCON using vmd to visualise the last frame of your simulation now "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!vmd -dlpolyconfig w40/REVCON"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "now inspect the text output of your simulation, replace kate with your favourite graphical text editor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!kate w40/OUTPUT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat the simulation at a different state point, where you might expect a phase transition to occur. Examine the result graphically once again (using the REVCON file) and try to deduce how the phase transition occurred. Look at the RDF plots and try to determine what phase the structure is now in.\n",
    "\n",
    "as previously you load the files and setup a new working directory.\n",
    "change the pressure and temperature to the desired values (60 for pressure, 500 for temperature) and rerun"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "dlPoly = DLPoly(control=\"CONTROL\", config=\"CONFIG\",\n",
    "                field=\"FIELD\", workdir=\"w20\")\n",
    "dlPoly.control.pres = 60\n",
    "dlPoly.control.temp = 500\n",
    "dlPoly.run(numProcs = 1)\n",
    "showrdf(\"w20/RDFDAT\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you do not see a phase transition, experiment with the control parameters (e.g. change the relaxation times, temperature or pressure, as you think fit) until you see one. Be as systematic as you can, using whatever insight you gain to rationalise what’s going on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dlPoly = DLPoly(control=\"CONTROL\", config=\"CONFIG\",\n",
    "                field=\"FIELD\", workdir=\"w20\")\n",
    "ctl=dlPoly.control\n",
    "ctl.pres=60\n",
    "ctl.temp=800\n",
    "ctl.ensemble.args=(0.1,1.0)\n",
    "dlPoly.run(numProcs = 1)\n",
    "showrdf(\"w20/RDFDAT\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dlpoly",
   "language": "python",
   "name": "dlpoly"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
